import { JsonController, Get, Post, Body } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService();

export interface StatusInput {
    uuid: String
    newStatus: String
}

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Post('/status')
    async updateStatus(@Body() statusInput: StatusInput) {
        return {
            status: 200,
            data: await flightsService.updateStatus(statusInput),
        }
    }
}
